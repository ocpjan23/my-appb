import moment from 'moment';

export const ControlsComponent = {
    selector: 'controls',
    template: require('./controls.template.html'),
    controller: class ControlsComponent {
        constructor() {
            'ngInject';

            let self = this;
            
            self.form = {
                year: moment().year(),
                month: moment().month()
            };
            self.toDate = {};
            self.data = [];

        }

        $onInit() {
            let self = this;
        }

        getCalendarList(_year, _month) {
            let self = this;
            let tempDate = moment().year(_year).month(_month);
            gapi.client.calendar.events.list({
                'calendarId': 'primary',
                'timeMin': (tempDate.startOf('month').toDate()).toISOString(),
                'timeMax': (tempDate.endOf('month').toDate()).toISOString(),
                'maxResults': 100
            }).then((r)=>{
                self.data = r.result.items;
                self.toDate = self.form;

            });
        }

        filter() {
            let self = this;
            console.log("filter");

            self.getCalendarList(self.form.year, self.form.month);

        }
    }
}