import _ from 'lodash';
import moment from 'moment';

export const CalendarComponent = {
    selector: 'calendar',
    template: require('./calendar.template.html'),
    bindings: {
      data: '=',
      toDate: '='
    },
    controller: class CalendarComponent {
        constructor($scope) {
            'ngInject';

            let self = this;

            self.year = 2019;
            self.month = 8;
            self.calData = [];
            self.copyData = null;

            $scope.$watch('$ctrl.data', ()=>{
                self.copyData = null;
                if (self.toDate.year &&
                    self.toDate.month && 
                    self.copyData !== self.data) {
                        self.copyData = self.data;
                        self.year = self.toDate.year;
                        self.month = self.toDate.month;
                }
                self._createCalendar(self.year, self.month, self.copyData);
            })
        }

        $onInit() {
            let self = this;

            console.log("Calendar World");
            self._createCalendar(self.year, self.month, null);
        }

        get() {
            let self = this;

            self._createCalendar(self.year, self.month, self.data);
        }

        _createCalendar(_year, _month, data) {
            let self = this;
            console.log("_createCalendar", _year);

            let controlDate = new Date(_year, _month + 1, 0);
            let currDate = new Date(_year, _month, 1);
            let iter = 0;
            let ready = true;
            let trObj = null;

            if (currDate.getDay() !== 0) {
                iter = 0 - currDate.getDay();
            }

            self.calData = [];
            while (ready) {
                if ( currDate.getDay() === 6) {
                    if (trObj) {
                        self.calData.push(trObj);
                        if(trObj.tdObj.length === 8) {
                            _.each(trObj.tdObj,(v,k)=>{
                                if(k === 0) {
                                    trObj.tdObj[k].weekFirstDate = trObj.tdObj[1];
                                    trObj.tdObj[k].weekLastDate = trObj.tdObj[7];
                                }
                            });
                        }
                    }
                    trObj = null;
                }

                if (!trObj) {
                    trObj = {};
                    trObj.tdObj = [];
                }

                currDate = new Date(_year, _month, ++iter);
                
                trObj.tdObj.push(self._newDayCell(currDate, iter < 1 || +currDate > +controlDate, data));

                if (+controlDate < +currDate && currDate.getDay() === 0) {
                    ready = false;
                }
            }
        }

        _newDayCell(_dateObj, _isOffset, data) {
            let self = this;
            let tdObj = {};

            tdObj.contents = [];
            if(data) {
                for (let i=0; i< data.length; i++) {

                    if (moment(_dateObj).get('date') === moment(data[i].start).get('date') &&
                        moment(_dateObj).get('month') === moment(data[i].start).get('month') &&
                        moment(_dateObj).get('year') === moment(data[i].start).get('year')) {
                        console.log(data[i]);
                        tdObj.summary = data[i].summary;
                    }
                }
            }

            tdObj.day =  _dateObj.getDate();
            tdObj.class = _isOffset ? 'day adj-month' : 'day';

            return tdObj;
        }
    }
}