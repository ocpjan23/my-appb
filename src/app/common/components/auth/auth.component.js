const CLIENT_ID = '642563121002-7lsvjc5hfnvu6lkpvlsaevfl5qpscgdh.apps.googleusercontent.com';
const API_KEY = 'AIzaSyDav1wZFBvlDpcQ9dqiATDL6sn_jLBTAFU';
const DISCOVERY_DOCS = ["https://www.googleapis.com/discovery/v1/apis/calendar/v3/rest"];
const SCOPES = "https://www.googleapis.com/auth/calendar";

import moment from 'moment'

export const AuthComponent = {
    selector: 'auth',
    template: require('./auth.template.html'),
    controller: class AuthComponent {
        constructor($scope) {
          'ngInject';
          let self = this;

          window.onSignIn = self.onSignIn;
          window.initClient = self.initClient;
          window.getcalendarList = self.getcalendarList;

          self.myTimeZone = "Asia/Manila";
          self.isConnected = false;
        }

        $onInit() {
            let self = this;

        }

        onSignIn(googleUser) {
          var profile = googleUser.getBasicProfile();
          
          console.log('ID: ' + profile.getId());
          console.log('Name: ' + profile.getName());
          console.log('Image URL: ' + profile.getImageUrl());
          console.log('Email: ' + profile.getEmail());

          gapi.load('client:auth2', initClient);
        }

        initClient() {
          gapi.client.init({
            apiKey: API_KEY,
            clientId: CLIENT_ID,
            discoveryDocs: DISCOVERY_DOCS,
            scope: SCOPES
          }).then(function (r) {
            console.log("Connected", r);
          }, function(error) {
            console.log(error)
          });
        }

        signIn() {
          let self = this;
          console.log("initClient");
          self.isConnected = true;
          self.initClient();
        }

        insert() {
          let self = this;
          let tempData = {
            'summary': 'HELLO WORLD',
            'location': 'room1',
            'attendees': [
              {'email': 'lpage@example.com'},
              {'email': 'sbrin@example.com'},
            ],
            "start": {
              "dateTime": moment().hours(1).minutes(0).format('YYYY-MM-DDTHH:mm:ss'),
              "timeZone": tselfhis.myTimeZone
            },
            "end": {
              "dateTime": moment().hours(2).minutes(0).format('YYYY-MM-DDTHH:mm:ss'),
              "timeZone": self.myTimeZone
            }
          }
          gapi.client.calendar.events.insert({
            'calendarId': 'primary',
            'resource': tempData
          }).then((r)=>{
            console.log(r);
          })
        }

        signOut() {
          var auth2 = gapi.auth2.getAuthInstance();
          auth2.signOut().then(function () {
            console.log('User signed out.');
          });
        }

    }
}