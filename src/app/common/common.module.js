import angular from 'angular';

// containers
import { AppComponent } from './containers/app/app.component';
// components
import { AppNavComponent } from './components/app-nav/app-nav.component';


import { AuthComponent } from './components/auth/auth.component';
import { ControlsComponent } from './components/controls/controls.component';
import { CalendarComponent } from './components/calendar/calendar.component';

// styles
import './components/app-nav/app-nav.component.scss';
import './components/controls/controls.component.scss';
import './components/calendar/calendar.component.scss';

const MODULE_NAME = 'common';
const MODULE_IMPORTS = [];

export const CommonModule = angular
  .module(MODULE_NAME, MODULE_IMPORTS)
  .component(AppComponent.selector, AppComponent)
  .component(AppNavComponent.selector, AppNavComponent)
  .component(AuthComponent.selector, AuthComponent)
  .component(ControlsComponent.selector, ControlsComponent)
  .component(CalendarComponent.selector, CalendarComponent)
  .config(($stateProvider, $locationProvider, $urlRouterProvider) => {
    'ngInject';

    $stateProvider
      .state(AppComponent.selector, {
        url: `/${AppComponent.selector}`,
        component: AppComponent.selector,
        data: {
          requiredAuth: true
        }
      });

    $locationProvider.hashPrefix('');
    $locationProvider.html5Mode(true);

    $urlRouterProvider.otherwise('/app');
  })
  .name;
